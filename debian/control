Source: ejabberd-contrib
Section: net
Priority: optional
Maintainer: Ejabberd Packaging Team <ejabberd@packages.debian.org>
Uploaders: Philipp Huebner <debalance@debian.org>
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               ejabberd (>= 24.12~),
               erlang-base,
               erlang-dev,
               erlang-tools,
               rebar
Standards-Version: 4.7.0
Homepage: https://github.com/processone/ejabberd-contrib
Vcs-Git: https://salsa.debian.org/ejabberd-packaging-team/ejabberd-contrib.git
Vcs-Browser: https://salsa.debian.org/ejabberd-packaging-team/ejabberd-contrib

Package: ejabberd-contrib
Architecture: all
Depends: ejabberd (=${ejabberd}),
         ejabberd-mod-cron,
         ejabberd-mod-default-contacts,
         ejabberd-mod-default-rooms,
         ejabberd-mod-deny-omemo,
         ejabberd-mod-filter,
         ejabberd-mod-grafite,
         ejabberd-mod-irc,
         ejabberd-mod-isolation,
         ejabberd-mod-log-chat,
         ejabberd-mod-logsession,
         ejabberd-mod-logxml,
         ejabberd-mod-message-log,
         ejabberd-mod-muc-log-http,
         ejabberd-mod-post-log,
         ejabberd-mod-pottymouth,
         ejabberd-mod-rest,
         ejabberd-mod-s2s-log,
         ejabberd-mod-shcommands,
         ejabberd-mod-spam-filter,
         ejabberd-mod-statsdx,
         ejabberd-mod-webpresence,
         erlang-base,
         ${erlang-abi:Depends},
         ${erlang:Depends},
         ${misc:Depends}
Description: user-contributed modules for ejabberd (metapackage)
 This package depends on all available modules from ejabberd-contrib,
 but doesn't do or add anything itself.
 .
 This is a pure metapackage.

Package: ejabberd-mod-cron
Architecture: any
Multi-Arch: allowed
Depends: ejabberd (=${ejabberd}),
         erlang-base,
         ${erlang-abi:Depends},
         ${erlang:Depends},
         ${misc:Depends}
Description: ejabberd module to execute scheduled commands
 This module allows advanced ejabberd administrators to schedule commands for
 periodic and automatic execution. Each time a scheduled task finishes its
 execution, a message is printed in the ejabberd log file.

Package: ejabberd-mod-default-contacts
Architecture: any
Multi-Arch: allowed
Depends: ejabberd (=${ejabberd}),
         erlang-base,
         ${erlang-abi:Depends},
         ${erlang:Depends},
         ${misc:Depends}
Description: ejabberd module to add roster contact(s) upon registration
 This module allows for specifying one or more contacts that should be
 added to the user's roster automatically on successful registration
 (via "mod_register", or, for example, "ejabberdctl register").
 Note that no presence subscription is performed, and the rosters of
 the contacts are not  modified.

Package: ejabberd-mod-default-rooms
Architecture: any
Multi-Arch: allowed
Depends: ejabberd (=${ejabberd}),
         erlang-base,
         ${erlang-abi:Depends},
         ${erlang:Depends},
         ${misc:Depends}
Description: ejabberd module to add MUC bookmark(s) upon registration
 This module allows for specifying one or more rooms that should be
 bookmarked automatically on successful user registration (via
 "mod_register", or, for example, "ejabberdctl register").

Package: ejabberd-mod-deny-omemo
Architecture: any
Multi-Arch: allowed
Depends: ejabberd (=${ejabberd}),
         erlang-base,
         ${erlang-abi:Depends},
         ${erlang:Depends},
         ${misc:Depends}
Description: ejabberd module to prevent OMEMO sessions from being established
 Unless the configured access rule (called 'omemo' by default) returns
 'allow', this module prevents OMEMO sessions from being established.
 Requests to query the devicelist from other users are rejected.  Requests
 to publish a devicelist are also rejected, and all OMEMO nodes referenced
 in that devicelist are removed.  Incoming devicelist updates are silently
 dropped.  OMEMO-encrypted messages are bounced with an error message.

Package: ejabberd-mod-filter
Architecture: any
Multi-Arch: allowed
Depends: ejabberd (=${ejabberd}),
         erlang-base,
         ${erlang-abi:Depends},
         ${erlang:Depends},
         ${misc:Depends}
Description: ejabberd module to allow specifying packet filtering rules using ACL and ACCESS
 This module allows the admin to specify packet filtering rules using
 ACL and ACCESS.
 The configuration of rules is done using ejabberd's ACL and ACCESS,
 so you should also study the corresponding section of the ejabberd guide.

Package: ejabberd-mod-grafite
Architecture: any
Multi-Arch: allowed
Depends: ejabberd (=${ejabberd}),
         erlang-base,
         ${erlang-abi:Depends},
         ${erlang:Depends},
         ${misc:Depends}
Description: ejabberd module to gather statistics and publish them via statsd/grafite
 This module gathers statistics from ejabberd and publishes them via
 statsd/grafite.
 Note that statsd/grafite must be set up separately.

Package: ejabberd-mod-irc
Architecture: any
Multi-Arch: allowed
Depends: ejabberd (=${ejabberd}),
         erlang-base,
         ${erlang-abi:Depends},
         ${erlang:Depends},
         ${misc:Depends}
Description: ejabberd module providing an IRC transport
 This module adds an IRC transport to ejabberd that can be used to join channels
 on IRC servers.
 Note that an IRC server must be provided separately.

Package: ejabberd-mod-isolation
Architecture: any
Multi-Arch: allowed
Depends: ejabberd (=${ejabberd}),
         erlang-base,
         ${erlang-abi:Depends},
         ${erlang:Depends},
         ${misc:Depends}
Description: ejabberd module to block communication of users between different virtual hosts
 This module blocks communication of users between different virtual hosts on
 the same ejabberd instance.
 The module doesn't have any options, it just needs to be enabled.

Package: ejabberd-mod-log-chat
Architecture: any
Multi-Arch: allowed
Depends: ejabberd (=${ejabberd}),
         erlang-base,
         ${erlang-abi:Depends},
         ${erlang:Depends},
         ${misc:Depends}
Description: ejabberd module to log chat messages to text or html
 mod_log_chat is an ejabberd module aimed at logging chat messages in
 text files. mod_log_chat creates one file per couple of chatters and
 per day (it doesn't log muc messages, use mod_muc_log for this).
 .
 It can store messages in plain text or HTML format.

Package: ejabberd-mod-logsession
Architecture: any
Multi-Arch: allowed
Depends: ejabberd (=${ejabberd}),
         erlang-base,
         ${erlang-abi:Depends},
         ${erlang:Depends},
         ${misc:Depends}
Description: ejabberd module to log session connections to a text file
 This module is intended to log the session connections in a text file.
 Right now it only logs the forbidden connection attempts and the
 failed authentication attempts.
 .
 Each vhost is logged in a different file.

Package: ejabberd-mod-logxml
Architecture: any
Multi-Arch: allowed
Depends: ejabberd (=${ejabberd}),
         erlang-base,
         ${erlang-abi:Depends},
         ${erlang:Depends},
         ${misc:Depends}
Description: ejabberd module to log XMPP packets to an XML file
 This module sniffs all the XMPP traffic sent and received by ejabberd,
 both internally and externally transmitted. It logs the XMPP packets
 to an XML formatted file. It's possible to filter transmitted packets
 by orientation, stanza and direction. It's possible to configure the
 file rotation rules and intervals.

Package: ejabberd-mod-message-log
Architecture: any
Multi-Arch: allowed
Depends: ejabberd (=${ejabberd}),
         erlang-base,
         ${erlang-abi:Depends},
         ${erlang:Depends},
         ${misc:Depends}
Description: ejabberd module to write a line for each message to a log file
 This module writes a line for each sent or received message to a log file.
 Each line mentions the sender's JID and the recipient's JID, and also the
 message type (e.g., "normal", "chat", or "groupchat").  Carbon copies are
 marked as such.

Package: ejabberd-mod-muc-log-http
Architecture: any
Multi-Arch: allowed
Depends: ejabberd (=${ejabberd}),
         erlang-base,
         ${erlang-abi:Depends},
         ${erlang:Depends},
         ${misc:Depends}
Description: ejabberd module to serve MUC logs on the web
 This module serves the directory containing MUC logs already configured on
 mod_muc_log. This way there is no need to set up a separate web server to
 allow your users to view the MUC logs.  It is a small modification of
 mod_http_fileserver, customized for log serving.

Package: ejabberd-mod-post-log
Architecture: any
Multi-Arch: allowed
Depends: ejabberd (=${ejabberd}),
         erlang-base,
         ${erlang-abi:Depends},
         ${erlang:Depends},
         ${misc:Depends}
Description: ejabberd module to log all messages to an HTTP API
 This module implements logging of all messages sent (chat and groupchat) via
 an HTTP API.
 Simply activate the module in ejabberd.yml after installation, see the included
 README.txt for further information.

Package: ejabberd-mod-pottymouth
Architecture: any
Multi-Arch: allowed
Depends: ejabberd (=${ejabberd}),
         erlang-base,
         ${erlang-abi:Depends},
         ${erlang:Depends},
         ${misc:Depends}
Description: ejabberd module to filter messages using blacklists
 The 'mod_pottymouth' ejabberd module aims to fill the void left by 'mod_shit'
 which has disappeared from the net. It allows individual whole words of a
 message to be filtered against a blacklist. It allows multiple blacklists
 sharded by language. To make use of this module the client must add the
 xml:lang attribute to the message xml.

Package: ejabberd-mod-rest
Architecture: any
Multi-Arch: allowed
Depends: ejabberd (=${ejabberd}),
         erlang-base,
         ${erlang-abi:Depends},
         ${erlang:Depends},
         ${misc:Depends}
Description: ejabberd module providing an HTTP REST interface
 This is an ejabberd module that adds an HTTP handler that allows HTTP
 clients to literally post arbitrary message stanzas to ejabberd. Those
 stanzas then get shoved through ejabberd's router just like any other
 stanza.
 .
 This module can also be used as a frontend to execute ejabberd commands.

Package: ejabberd-mod-s2s-log
Architecture: any
Multi-Arch: allowed
Depends: ejabberd (=${ejabberd}),
         erlang-base,
         ${erlang-abi:Depends},
         ${erlang:Depends},
         ${misc:Depends}
Description: ejabberd module to log XMPP s2s connections
 This module can be used to keep a track of other XMPP servers your server has
 been connected with.
 Simply activate the module in ejabberd.yml after installation, see the included
 README.txt for further information.

Package: ejabberd-mod-shcommands
Architecture: any
Multi-Arch: allowed
Depends: ejabberd (=${ejabberd}),
         erlang-base,
         ${erlang-abi:Depends},
         ${erlang:Depends},
         ${misc:Depends}
Description: ejabberd module to execute shell commands via XMPP (dangerous!)
 WARNING: USE THIS MODULE AT YOUR OWN RISK !!!
 This module allows ejabberd administrators to remotely execute shell commands
 which could compromise both the ejabberd server and the whole machine.
 .
 This module provides the ejabberd server administrator a method to remotely
 execute shell commands through the ejabberd server.
 .
 It provides a page in the ejabberd Web Admin which only the administrators of
 the whole server can access.
 .
 Three types of commands are possible:
  * ejabberd_ctl: makes a call to ejabberd_ctl;
  * erlang shell: executes an erlang command;
  * system shell: executes a command on the system shell.
 The result of the execution will be shown.
 .
 In the system shell, only non-interactive commands will work correctly,
 for example this will work:
  ps -all
 Don't use commands that start an interactive mode:
 DON'T TRY THIS: top
 DON'T TRY THIS: vim readme.txt
 .
 This module does not check if the commands are dangerous or problematic,
 so this module is only recommended for experienced ejabberd and Erlang/OTP
 administrators. USE THIS MODULE AT YOUR OWN RISK !!!

Package: ejabberd-mod-spam-filter
Architecture: any
Multi-Arch: allowed
Depends: ejabberd (=${ejabberd}),
         erlang-base,
         ${erlang-abi:Depends},
         ${erlang:Depends},
         ${misc:Depends}
Description: ejabberd module to filter spam messages based on JID/content
 This module allows for filtering spam messages and subscription requests
 received from remote servers based on lists of known spammer JIDs and/or
 URLs mentioned in spam messages. Traffic classified as spam is rejected
 with an error (and an [info] message is logged) unless the sender is
 subscribed to the recipient's presence.  An access rule can be specified
 to control which recipients are subject to spam filtering

Package: ejabberd-mod-statsdx
Architecture: any
Multi-Arch: allowed
Depends: ejabberd (=${ejabberd}),
         erlang-base,
         ${erlang-abi:Depends},
         ${erlang:Depends},
         ${misc:Depends}
Description: ejabberd module to calculate and gather statistics actively
 This module measures several statistics. It provides a new section in
 ejabberd Web Admin and two ejabberd commands to view the information.
 Simply activate the module in ejabberd.yml after installation, see the included
 README.txt for further information.

Package: ejabberd-mod-webpresence
Architecture: any
Multi-Arch: allowed
Depends: ejabberd (=${ejabberd}),
         erlang-base,
         ${erlang-abi:Depends},
         ${erlang:Depends},
         ${misc:Depends}
Description: ejabberd module allowing users to publish their presence information
 This module allows any local user of the ejabberd server to publish his
 presence information on the web.
 This module is the successor of Igor Goryachev's mod_presence.
 .
 Allowed output methods are:
  * icons (various themes available);
  * status text;
  * raw XML;
  * avatar, stored in the user's vCard.
 .
 No web server, database, additional libraries or programs are required.
